import json
import jsonschema
import requests_cache
import yaml
from constants import contact_fields
from datetime import datetime, timedelta
from defusedxml import ElementTree
from flask import Flask, Response, make_response, render_template, request
from flask_bootstrap import Bootstrap5
from flask_fontawesome import FontAwesome
from urllib.parse import quote_plus, unquote_plus

SPACEAPI_DIRECTORY_URL = "https://directory.spaceapi.io/"
app = Flask(__name__)
app.config.from_file("config.yaml", load=yaml.safe_load)
bootstrap = Bootstrap5(app)
fa = FontAwesome(app)
session = requests_cache.CachedSession("hackerspaces-microwiki", expire_after=timedelta(days=1))

API_SCHEMAS = {
    12: "scripts/schema/12.json",
    13: "scripts/schema/13.json",
    14: "scripts/schema/14.json",
    15: "scripts/schema/15-draft.json"
}


def get_api_ver(spaceapi):
    if "api_compatibility" in spaceapi:
        if "14" in spaceapi["api_compatibility"]:
            return 14
        elif "15" in spaceapi["api_compatibility"]:  # 15 is a draft version, so checked after 14
            return 15
        elif "api" not in spaceapi:
            raise ValueError(f'API versions [{", ".join(spaceapi["api_compatibility"])}] not supported')
    if "api" in spaceapi:
        if spaceapi["api"] == "0.13":
            return 13
        elif spaceapi["api"] == "0.12":
            return 12
        else:
            raise ValueError(f'API version {spaceapi["api"]} not supported')
    else:
        raise KeyError("Keys 'api' and 'api_compatibility' not present in SpaceAPI JSON")


def test_url(url, key):
    code = session.get(url, timeout=1).status_code

    if code // 100 != 2:  # 2xx code
        raise RuntimeError(f"Error {code} when loading URL '{url}' from JSON key {key}")


def log_if_url_err(errs, url, key):
    try:
        test_url(url, key)
    except Exception as err:
        errs.append(err)


def test_space(spaceapi):
    errs = []

    try:
        api_version = get_api_ver(spaceapi)

        with open(API_SCHEMAS[api_version], "r") as schema_file:
            schema = json.load(schema_file)
            try:
                jsonschema.validate(instance=spaceapi, schema=schema)
            except Exception as e:
                errs.append(e)

        if "url" in spaceapi:
            log_if_url_err(errs, spaceapi["url"], "url")

        if "logo" in spaceapi:
            log_if_url_err(errs, spaceapi["logo"], "logo")

        if "cam" in spaceapi:
            i = 0
            for url in spaceapi["cam"]:
                log_if_url_err(errs, url, f"cam[{i}]")
                i += 1

        if "state" in spaceapi:
            if "icon" in spaceapi["state"]:
                if "open" in spaceapi["state"]["icon"]:
                    log_if_url_err(errs, spaceapi["state"]["icon"]["open"], "state.icon.open")
                if "closed" in spaceapi["state"]["icon"]:
                    log_if_url_err(errs, spaceapi["state"]["icon"]["closed"], "state.icon.closed")

        if "feeds" in spaceapi:
            for feed_key, feed_object in spaceapi["feeds"].items():
                if "url" in feed_object:
                    log_if_url_err(errs, feed_object["url"], f"feeds.{feed_key}.url")

        if "projects" in spaceapi:
            i = 0
            for url in spaceapi["projects"]:
                log_if_url_err(errs, url, f"projects[{i}]")
                i += 1

        if "links" in spaceapi:
            i = 0
            for link_object in spaceapi["links"]:
                log_if_url_err(errs, link_object["url"], f"links[{i}].url")
                i += 1
    except Exception as e:
        errs.append(e)

    return errs


def timestamp_to_string(timestamp):
    return datetime.utcfromtimestamp(timestamp).strftime("%d %B %Y %T UTC")


def double_encode(value):
    return quote_plus(quote_plus(value))


def double_decode(value):
    return unquote_plus(unquote_plus(value))


def fetch_feed(feed_text):
    try:
        feed_tree = ElementTree.fromstring(feed_text)

        output = "<ul>"
        for item in feed_tree.find("channel").findall("item"):
            output += f'<li><a href="{ item.find("link").text }" target="_blank">{ item.find("title").text }</a></li>'
        output += "</ul>"

        feed_title = feed_tree.find("channel").find("title").text
        return feed_title, output
    except Exception as e:
        return None, e


def fetch_with_error_handling(url_space_name):
    space_name = double_decode(url_space_name)
    spacedir = session.get(SPACEAPI_DIRECTORY_URL, timeout=1).json()

    try:
        url = spacedir[space_name]
        response = session.get(url, timeout=1)
        code = response.status_code

        if code // 100 == 2:  # 2xx code
            spaceapi = response.json()
            return 200, space_name, spacedir, spaceapi, None
        else:
            return code, space_name, spacedir, None, RuntimeError(f"Error {code} when loading URL '{url}'")
    except KeyError as e:
        return 404, space_name, spacedir, None, e
    except Exception as e:
        return 500, space_name, spacedir, None, e


@app.route("/")
def front_page():  # put application's code here
    spacedir = session.get(SPACEAPI_DIRECTORY_URL).json()
    return render_template("index.html",
                           hide_space_list_in_sidebar=True,
                           spacedir=spacedir,
                           urlencode=double_encode)

def err_to_html(err):
    if isinstance(err, jsonschema.ValidationError):
        return f"<pre>{str(err)}</pre>"
    else:
        return str(err)


@app.route("/wiki/<url_space_name>")
def space_details_page(url_space_name):
    code, space_name, spacedir, spaceapi, e = fetch_with_error_handling(url_space_name)

    if code == 200:
        errs = test_space(spaceapi)
        str_errs = list(map(err_to_html, errs))

        return make_response(render_template("wiki.html",
                                             code=200,
                                             title=spaceapi['space'],
                                             spacedir=spacedir,
                                             spaceapi=spaceapi,
                                             space_name=space_name,
                                             urlencode=double_encode,
                                             timestamp_to_string=timestamp_to_string,
                                             session=session,
                                             fetch_feed=fetch_feed,
                                             base_url=request.base_url,
                                             contact_fields=contact_fields,
                                             errs=str_errs), 200)

    print(e)
    return make_response(render_template("error.html",
                                         code=code,
                                         title=type(e).__name__,
                                         error_message=str(e)), code)


@app.route("/calendar/<url_space_name>")
def fetch_calendar(url_space_name):
    space_name = double_decode(url_space_name)
    spacedir = session.get(SPACEAPI_DIRECTORY_URL).json()
    try:
        spaceapi = session.get(spacedir[space_name]).json()
        calendar = session.get(spaceapi["feeds"]["calendar"]["url"]).content
        code = 200
    except KeyError:
        calendar = "404 Hackerspace not found"
        code = 404
    except Exception:
        calendar = "500 Internal Server Error"
        code = 500

    return Response(calendar, status=code, mimetype="text/calendar")


if __name__ == "__main__":
    app.run()
