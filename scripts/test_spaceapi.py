#!/usr/bin/env python3

import json
import jsonschema
import requests
from argparse import ArgumentParser
from logging import error, warning, info, debug, getLogger

API_SCHEMAS = {
    12: "scripts/schema/12.json",
    13: "scripts/schema/13.json",
    14: "scripts/schema/14.json",
    15: "scripts/schema/15-draft.json"
}


def get_api_ver(spaceapi):
    debug("Checking api_compatibility value")
    if "api_compatibility" in spaceapi:
        if "14" in spaceapi["api_compatibility"]:
            return 14
        elif "15" in spaceapi["api_compatibility"]:  # 15 is a draft version, so checked after 14
            return 15
        elif "api" not in spaceapi:
            raise ValueError(f'API versions [{", ".join(spaceapi["api_compatibility"])}] not supported')
    if "api" in spaceapi:
        if spaceapi["api"] == "0.13":
            return 13
        elif spaceapi["api"] == "0.12":
            return 12
        else:
            raise ValueError(f'API version {spaceapi["api"]} not supported')
    else:
        raise KeyError("Keys 'api' and 'api_compatibility' not present in SpaceAPI JSON")


def log_exception(errlevel, err, name, url):
    errlevel(f"In hackerspace '{name}' with URL '{url}':\n{repr(err)}")


def test_url(url, key):
    code = requests.get(url, timeout=1).status_code

    if code // 100 != 2:  # 2xx code
        raise RuntimeError(f"Error {code} when loading URL '{url}' from JSON key {key}")


def log_if_url_err(url, key, name):
    try:
        test_url(url, key)
    except Exception as err:
        log_exception(warning, err, name, url)


def test_space(name, url):
    info(f"Testing space {name}...")
    debug(f"Requesting {url}...")
    response = requests.get(url, timeout=1)
    code = response.status_code

    if code // 100 == 2:  # 2xx code
        spaceapi = response.json()
        debug("Success!")

        api_version = get_api_ver(spaceapi)  # Due to exceptions, guaranteed to return 12, 13, 14 or 15

        with open(API_SCHEMAS[api_version], "r") as schema_file:
            schema = json.load(schema_file)
            try:  # Even if there are errors in the specification, there is more to check
                jsonschema.validate(instance=spaceapi, schema=schema)
            except Exception as err:
                log_exception(warning, err, name, url)

        if "url" in spaceapi:
            log_if_url_err(spaceapi["url"], "url", name)

        if "logo" in spaceapi:
            log_if_url_err(spaceapi["logo"], "logo", name)

        if "cam" in spaceapi:
            i = 0
            for url in spaceapi["cam"]:
                log_if_url_err(url, f"cam[{i}]", name)
                i += 1

        if "state" in spaceapi:
            if "icon" in spaceapi["state"]:
                if "open" in spaceapi["state"]["icon"]:
                    log_if_url_err(spaceapi["state"]["icon"]["open"], "state.icon.open", name)
                if "closed" in spaceapi["state"]["icon"]:
                    log_if_url_err(spaceapi["state"]["icon"]["closed"], "state.icon.closed", name)

        if "feeds" in spaceapi:
            for feed_key, feed_object in spaceapi["feeds"].items():
                if "url" in feed_object:
                    log_if_url_err(feed_object["url"], f"feeds.{feed_key}.url", name)

        if "projects" in spaceapi:
            i = 0
            for url in spaceapi["projects"]:
                log_if_url_err(url, f"projects[{i}]", name)
                i += 1

        if "links" in spaceapi:
            i = 0
            for link_object in spaceapi["links"]:
                log_if_url_err(link_object["url"], f"links[{i}].url", name)
                i += 1
    else:
        log_exception(error, RuntimeError(f"Error {code} when loading URL '{url}'"), name, url)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-l", "--log")
    parser_args = vars(parser.parse_args())
    log_level = parser_args.get("log")

    if log_level is not None:
        if log_level.isdigit():
            log_level = int(log_level)

        getLogger().setLevel(log_level)

    info("Requesting SpaceAPI directory...")
    space_dir = requests.get("https://directory.spaceapi.io/").json()
    debug("Successfully retrieved SpaceAPI directory!")

    for space_name, space_url in space_dir.items():
        try:
            test_space(space_name, space_url)
        except Exception as e:
            log_exception(error, e, space_name, space_url)
