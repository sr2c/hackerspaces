class ContactField(object):
    def __init__(self, display_field, url_prefix=None, full_name=None, h_card_name=None):
        self.display_field = display_field
        self.full_name = full_name
        self.url_prefix = "" if url_prefix is None else url_prefix
        self.h_card_name = h_card_name

    def generate_link(self, value):
        output = "<li"

        if self.h_card_name is not None:
            output += f' class="card-text { self.h_card_name }"'

        output += ">"

        if self.full_name is not None:
            output += f'<abbr title="{ self.full_name }">'

        output += self.display_field

        if self.full_name is not None:
            output += "</abbr>"

        output += f': <a href="{ self.url_prefix }{ value }">{ value }</a></li>'

        return output


contact_fields = {
    "email": ContactField("Email", url_prefix="mailto:", h_card_name="u-email"),
    "ext_mastodon": ContactField("Mastodon"),
    "facebook": ContactField("Facebook"),
    "foursquare": ContactField("Foursquare"),
    "identica": ContactField("Identica"),
    "irc": ContactField("IRC", full_name="Internet Relay Chat"),
    "jabber": ContactField("XMPP", url_prefix="xmpp:", full_name="Extensible Messaging and Presence Protocol"),
    "ml": ContactField("Mailing List", url_prefix="mailto:"),
    "phone": ContactField("Phone", url_prefix="tel:"),
    "sip": ContactField("SIP", full_name="Session Initiation Protocol"),
    "twitter": ContactField("Twitter")
}
